import { ElementFinder, element, by } from "protractor";

export class Home {
    lnkExperience: ElementFinder;
    lnkActivities: ElementFinder;

    constructor() {
        this.lnkExperience = element(by.xpath("//a[@aria-label='The Experience']"))
        this.lnkActivities = element(by.xpath("//a[text()='Activities']"))
        
    }
}