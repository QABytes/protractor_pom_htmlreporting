import { ElementFinder, element, by } from "protractor";

export class Activity {
    viewAllButton: ElementFinder

    constructor() {
        this.viewAllButton = element(by.xpath("//a[@aria-label='Activities View All']"));

    }
}