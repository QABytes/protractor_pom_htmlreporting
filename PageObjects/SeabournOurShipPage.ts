import { ElementFinder, element, by } from "protractor";

export class OurShip {
    viewCruisesButton: ElementFinder

    constructor() {
        //wantedly made xpath as wrong to capture screenshot
        this.viewCruisesButton = element(by.xpath("(//a[@aria-label='Seabourn Pursuit View Cruises']"));

    }
}