import { ElementFinder, element, by } from "protractor";

export class SeaBournHome {
    lnkLifeonBoard: ElementFinder;
    lnkOurShip: ElementFinder;

    constructor() {
        this.lnkLifeonBoard = element(by.xpath("//a[@aria-label='Life On Board']"))
        this.lnkOurShip = element(by.xpath("//a[text()='Our Ships']"))
        
    }
}