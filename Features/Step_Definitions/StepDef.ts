import { Given, When, Then, Before, After, Status } from "cucumber"
import { browser, element, by, ExpectedConditions } from "protractor"
import chai from "chai";
import { Home } from "../../PageObjects/HomePage";
import { Activity } from "../../PageObjects/ActivityPage";
import { SeaBournHome } from "../../PageObjects/SeabournHomePage";
import { OurShip } from "../../PageObjects/SeabournOurShipPage";


var expect = chai.expect;
let ec = ExpectedConditions;

let objHome = new Home();
let objActivity = new Activity();

let objSBHome = new SeaBournHome();
let objSBShip = new OurShip();



Given('I launch the url {string}', { timeout: 60 * 1000 }, function (url) {
    browser.waitForAngularEnabled(false);
     browser.get(url).then(async function () {

    
    browser.driver.manage().window().maximize();
    browser.sleep(5000)
    })
})

Then('I verify the pagetitle {string}', {timeout: 60 * 1000}, async function (string) {
    // Write code here that turns the phrase above into concrete actions
  // browser.waitForAngularEnabled(false);
    let requireTitle = await browser.getTitle()
   
    await browser.wait(ec.titleContains(string),10000)
    console.log('Title',requireTitle)
    await expect(requireTitle).contains(string)

  });

  When('I click on The Experience Link', {timeout: 60 * 1000}, async function () {
    await objHome.lnkExperience.click();
  });

  Then('I click on OnboardActivities SubLink', {timeout: 60 * 1000},async function () {
    await browser.wait(ec.elementToBeClickable(objHome.lnkActivities),10000)
   
    await objHome.lnkActivities.click();
    //await browser.sleep(5000)
});

Then('I click on ViewAll button', { timeout: 60 * 1000 }, async function () {
  await browser.wait(ec.elementToBeClickable(objActivity.viewAllButton),10000)
    await objActivity.viewAllButton.click();
    await browser.sleep(5000)
});



When('I click on LifeonBoard Link', {timeout: 60 * 1000},async function () {
  
    await objSBHome.lnkLifeonBoard.click();
  });

  Then('I click on OurShip SubLink', {timeout: 60 * 1000},async function () {
    await browser.wait(ec.elementToBeClickable(objSBHome.lnkOurShip),10000)
    await objSBHome.lnkOurShip.click();
  });
  Then('I click on ViewCruises button', {timeout: 60 * 1000},async function () {
    await browser.wait(ec.elementToBeClickable(objSBShip.viewCruisesButton),10000)
    await objSBShip.viewCruisesButton.click();
  });

Before(async function () {
    await browser.manage().deleteAllCookies();
})

After(async function (scenario) {
    if (scenario.result.status === Status.FAILED) {
        const screenshot = await browser.takeScreenshot();
        this.attach(screenshot, "image/png")
    }
})