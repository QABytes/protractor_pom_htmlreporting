Feature: Assessment to Automate Holland America and Seabourn site

    Scenario Outline: To Navigate to Onboard Activities in Hollandamerica Site

        Given I launch the url "https://www.hollandamerica.com/en_US.html"
        Then I verify the pagetitle "<HomePageTitle>"
        When I click on The Experience Link
        Then I click on OnboardActivities SubLink
        And I verify the pagetitle "<ActivityPageTitle>"
        Then I click on ViewAll button

        Examples:
            | HomePageTitle | ActivityPageTitle |
            | Holland America | Activities   | 

    Scenario Outline: To Navigate to View All Crusies in Seabourn Site

        Given I launch the url "https://www.seabourn.com/en_US.html"
        Then I verify the pagetitle "<HomePageTitle>"
        When I click on LifeonBoard Link
        Then I click on OurShip SubLink
        And I verify the pagetitle "<OurShipTitle>"
        Then I click on ViewCruises button

        Examples:
            | HomePageTitle | OurShipTitle |
            | Seabourn | Small Ship Cruises   | 
