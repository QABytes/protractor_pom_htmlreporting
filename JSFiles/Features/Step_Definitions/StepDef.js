"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const chai_1 = __importDefault(require("chai"));
const HomePage_1 = require("../../PageObjects/HomePage");
const ActivityPage_1 = require("../../PageObjects/ActivityPage");
const SeabournHomePage_1 = require("../../PageObjects/SeabournHomePage");
const SeabournOurShipPage_1 = require("../../PageObjects/SeabournOurShipPage");
var expect = chai_1.default.expect;
let ec = protractor_1.ExpectedConditions;
let objHome = new HomePage_1.Home();
let objActivity = new ActivityPage_1.Activity();
let objSBHome = new SeabournHomePage_1.SeaBournHome();
let objSBShip = new SeabournOurShipPage_1.OurShip();
(0, cucumber_1.Given)('I launch the url {string}', { timeout: 60 * 1000 }, function (url) {
    protractor_1.browser.waitForAngularEnabled(false);
    protractor_1.browser.get(url).then(function () {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_1.browser.driver.manage().window().maximize();
            protractor_1.browser.sleep(5000);
        });
    });
});
(0, cucumber_1.Then)('I verify the pagetitle {string}', { timeout: 60 * 1000 }, function (string) {
    return __awaiter(this, void 0, void 0, function* () {
        // Write code here that turns the phrase above into concrete actions
        // browser.waitForAngularEnabled(false);
        let requireTitle = yield protractor_1.browser.getTitle();
        yield protractor_1.browser.wait(ec.titleContains(string), 10000);
        console.log('Title', requireTitle);
        yield expect(requireTitle).contains(string);
    });
});
(0, cucumber_1.When)('I click on The Experience Link', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield objHome.lnkExperience.click();
    });
});
(0, cucumber_1.Then)('I click on OnboardActivities SubLink', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.wait(ec.elementToBeClickable(objHome.lnkActivities), 10000);
        yield objHome.lnkActivities.click();
        //await browser.sleep(5000)
    });
});
(0, cucumber_1.Then)('I click on ViewAll button', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.wait(ec.elementToBeClickable(objActivity.viewAllButton), 10000);
        yield objActivity.viewAllButton.click();
        yield protractor_1.browser.sleep(5000);
    });
});
(0, cucumber_1.When)('I click on LifeonBoard Link', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield objSBHome.lnkLifeonBoard.click();
    });
});
(0, cucumber_1.Then)('I click on OurShip SubLink', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.wait(ec.elementToBeClickable(objSBHome.lnkOurShip), 10000);
        yield objSBHome.lnkOurShip.click();
    });
});
(0, cucumber_1.Then)('I click on ViewCruises button', { timeout: 60 * 1000 }, function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.wait(ec.elementToBeClickable(objSBShip.viewCruisesButton), 10000);
        yield objSBShip.viewCruisesButton.click();
    });
});
(0, cucumber_1.Before)(function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.manage().deleteAllCookies();
    });
});
(0, cucumber_1.After)(function (scenario) {
    return __awaiter(this, void 0, void 0, function* () {
        if (scenario.result.status === cucumber_1.Status.FAILED) {
            const screenshot = yield protractor_1.browser.takeScreenshot();
            this.attach(screenshot, "image/png");
        }
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RlcERlZi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL0ZlYXR1cmVzL1N0ZXBfRGVmaW5pdGlvbnMvU3RlcERlZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUFtRTtBQUNuRSwyQ0FBcUU7QUFDckUsZ0RBQXdCO0FBQ3hCLHlEQUFrRDtBQUNsRCxpRUFBMEQ7QUFDMUQseUVBQWtFO0FBQ2xFLCtFQUFnRTtBQUdoRSxJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBQ3pCLElBQUksRUFBRSxHQUFHLCtCQUFrQixDQUFDO0FBRTVCLElBQUksT0FBTyxHQUFHLElBQUksZUFBSSxFQUFFLENBQUM7QUFDekIsSUFBSSxXQUFXLEdBQUcsSUFBSSx1QkFBUSxFQUFFLENBQUM7QUFFakMsSUFBSSxTQUFTLEdBQUcsSUFBSSwrQkFBWSxFQUFFLENBQUM7QUFDbkMsSUFBSSxTQUFTLEdBQUcsSUFBSSw2QkFBTyxFQUFFLENBQUM7QUFJOUIsSUFBQSxnQkFBSyxFQUFDLDJCQUEyQixFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsR0FBRyxJQUFJLEVBQUUsRUFBRSxVQUFVLEdBQUc7SUFDcEUsb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNwQyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7O1lBR3ZCLG9CQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzVDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ25CLENBQUM7S0FBQSxDQUFDLENBQUE7QUFDTixDQUFDLENBQUMsQ0FBQTtBQUVGLElBQUEsZUFBSSxFQUFDLGlDQUFpQyxFQUFFLEVBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxJQUFJLEVBQUMsRUFBRSxVQUFnQixNQUFNOztRQUNoRixvRUFBb0U7UUFDdEUsd0NBQXdDO1FBQ3RDLElBQUksWUFBWSxHQUFHLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQTtRQUUzQyxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEVBQUMsS0FBSyxDQUFDLENBQUE7UUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUMsWUFBWSxDQUFDLENBQUE7UUFDakMsTUFBTSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBRTdDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxJQUFBLGVBQUksRUFBQyxnQ0FBZ0MsRUFBRSxFQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsSUFBSSxFQUFDLEVBQUU7O1FBQzNELE1BQU0sT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsSUFBQSxlQUFJLEVBQUMsc0NBQXNDLEVBQUUsRUFBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLElBQUksRUFBQyxFQUFDOztRQUNoRSxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUMsS0FBSyxDQUFDLENBQUE7UUFFeEUsTUFBTSxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BDLDJCQUEyQjtJQUMvQixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsSUFBQSxlQUFJLEVBQUMsMkJBQTJCLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxHQUFHLElBQUksRUFBRSxFQUFFOztRQUN4RCxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEVBQUMsS0FBSyxDQUFDLENBQUE7UUFDMUUsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDN0IsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUlILElBQUEsZUFBSSxFQUFDLDZCQUE2QixFQUFFLEVBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxJQUFJLEVBQUMsRUFBQzs7UUFFckQsTUFBTSxTQUFTLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3pDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxJQUFBLGVBQUksRUFBQyw0QkFBNEIsRUFBRSxFQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsSUFBSSxFQUFDLEVBQUM7O1FBQ3RELE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBQyxLQUFLLENBQUMsQ0FBQTtRQUN2RSxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDckMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILElBQUEsZUFBSSxFQUFDLCtCQUErQixFQUFFLEVBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxJQUFJLEVBQUMsRUFBQzs7UUFDekQsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUMsS0FBSyxDQUFDLENBQUE7UUFDOUUsTUFBTSxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDNUMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVMLElBQUEsaUJBQU0sRUFBQzs7UUFDSCxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM5QyxDQUFDO0NBQUEsQ0FBQyxDQUFBO0FBRUYsSUFBQSxnQkFBSyxFQUFDLFVBQWdCLFFBQVE7O1FBQzFCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssaUJBQU0sQ0FBQyxNQUFNLEVBQUU7WUFDMUMsTUFBTSxVQUFVLEdBQUcsTUFBTSxvQkFBTyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFBO1NBQ3ZDO0lBQ0wsQ0FBQztDQUFBLENBQUMsQ0FBQSJ9