"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Activity = void 0;
const protractor_1 = require("protractor");
class Activity {
    constructor() {
        this.viewAllButton = (0, protractor_1.element)(protractor_1.by.xpath("//a[@aria-label='Activities View All']"));
    }
}
exports.Activity = Activity;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWN0aXZpdHlQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vUGFnZU9iamVjdHMvQWN0aXZpdHlQYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUF3RDtBQUV4RCxNQUFhLFFBQVE7SUFHakI7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUEsb0JBQU8sRUFBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUMsQ0FBQztJQUVyRixDQUFDO0NBQ0o7QUFQRCw0QkFPQyJ9