"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeaBournHome = void 0;
const protractor_1 = require("protractor");
class SeaBournHome {
    constructor() {
        this.lnkLifeonBoard = (0, protractor_1.element)(protractor_1.by.xpath("//a[@aria-label='Life On Board']"));
        this.lnkOurShip = (0, protractor_1.element)(protractor_1.by.xpath("//a[text()='Our Ships']"));
    }
}
exports.SeaBournHome = SeaBournHome;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhYm91cm5Ib21lUGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL1BhZ2VPYmplY3RzL1NlYWJvdXJuSG9tZVBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsWUFBWTtJQUlyQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBQSxvQkFBTyxFQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFBO1FBQzNFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBQSxvQkFBTyxFQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFBO0lBRWxFLENBQUM7Q0FDSjtBQVRELG9DQVNDIn0=