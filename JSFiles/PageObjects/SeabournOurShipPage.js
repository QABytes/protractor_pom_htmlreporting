"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OurShip = void 0;
const protractor_1 = require("protractor");
class OurShip {
    constructor() {
        //wantedly made xpath as wrong to capture screenshot
        this.viewCruisesButton = (0, protractor_1.element)(protractor_1.by.xpath("(//a[@aria-label='Seabourn Pursuit View Cruises']"));
    }
}
exports.OurShip = OurShip;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhYm91cm5PdXJTaGlwUGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL1BhZ2VPYmplY3RzL1NlYWJvdXJuT3VyU2hpcFBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsT0FBTztJQUdoQjtRQUNJLG9EQUFvRDtRQUNwRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBQSxvQkFBTyxFQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQyxDQUFDO0lBRXBHLENBQUM7Q0FDSjtBQVJELDBCQVFDIn0=