"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Activity = void 0;
const protractor_1 = require("protractor");
class Activity {
    constructor() {
        this.viewAllButton = (0, protractor_1.element)(protractor_1.by.xpath("//a[@aria-label='Activities View All']"));
    }
}
exports.Activity = Activity;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWN0aXZpdHkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9QYWdlT2JqZWN0cy9BY3Rpdml0eS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBd0Q7QUFFeEQsTUFBYSxRQUFRO0lBR2pCO1FBQ0ksSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFBLG9CQUFPLEVBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLENBQUM7SUFFckYsQ0FBQztDQUNKO0FBUEQsNEJBT0MifQ==